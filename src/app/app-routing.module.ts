import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TablePageComponent} from './page/table/table-page/table-page.component';

const routes: Routes = [
  {path: '', component: TablePageComponent},
  {path: 'tablePage', component: TablePageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
